<?php

namespace Drupal\uw_ct_sidebar\EventSubscriber;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\core_event_dispatcher\EntityHookEvents;
use Drupal\core_event_dispatcher\Event\Entity\EntityInsertEvent;
use Drupal\core_event_dispatcher\Event\Entity\EntityUpdateEvent;
use Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent;
use Drupal\node\NodeInterface;
use Drupal\preprocess_event_dispatcher\Event\NodePreprocessEvent;
use Drupal\uw_cfg_common\Service\UWServiceInterface;
use Drupal\uw_custom_blocks\CustomBlocks\UwCblBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class Uw Sidebar Event Subscriber.
 */
class UwCtSidebarEventSubscriber extends UwCblBase implements EventSubscriberInterface {
  use StringTranslationTrait;

  /**
   * Variable for the entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The variable for UW service.
   *
   * @var \Drupal\uw_cfg_common\Service\UWServiceInterface
   */
  protected $uwService;

  /**
   * The variable for route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The variable for Drupal messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Default constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity Type Manager from core.
   * @param \Drupal\uw_cfg_common\Service\UWServiceInterface $uwService
   *   Custom UW service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match entity.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Drupal messenger.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, UWServiceInterface $uwService, RouteMatchInterface $routeMatch, Messenger $messenger) {
    $this->entityTypeManager = $entityTypeManager;
    $this->uwService = $uwService;
    $this->routeMatch = $routeMatch;
    $this->messenger = $messenger;
  }

  /**
   * Alter node form.
   *
   * @param \Drupal\core_event_dispatcher\Event\Form\FormBaseAlterEvent $event
   *   The event.
   */
  public function alterNodeForm(FormBaseAlterEvent $event): void {

    // Get the form from the event.
    $form = &$event->getForm();

    // Switch on the form_id to set/unset specific things.
    switch ($form['#form_id']) {

      // If we are on the create or edit or clone sidebar node page,
      // hide title, set entity builders and add validation.
      case 'node_uw_ct_sidebar_form':
      case 'node_uw_ct_sidebar_edit_form':
      case 'node_uw_ct_sidebar_quick_node_clone_form':

        // Hide title when creating a new sidebar node.
        $form['title']['#access'] = FALSE;

        // Set a title when creating a new sidebar node.
        $form['#entity_builders'][] = [$this, 'uwCtSidebarNodeBuilder'];

        // Add the validation for field_uw_attach_page.
        $form['#validate'][] = [$this, 'uwCtSidebarAttachPageValidation'];
        break;

      case 'node_uw_ct_blog_layout_builder_form':
      case 'node_uw_ct_event_layout_builder_form':
      case 'node_uw_ct_news_item_layout_builder_form':
      case 'node_uw_ct_web_page_layout_builder_form':
      case 'node_uw_ct_blog_edit_form':
      case 'node_uw_ct_event_edit_form':
      case 'node_uw_ct_news_item_edit_form':
      case 'node_uw_ct_web_page_edit_form':
        // Load the current node.
        $node = $this->routeMatch->getParameter('node');

        // Get the sidebar_nid.
        $sidebar_nid = $this->uwService->getOrCheckAttachedSidebar($node->id());

        // If we have a sidebar_nid that is greater than 0, meaning there
        // is a sidebar attached to this node, then add message to top of node.
        if ($sidebar_nid > 0) {

          // Get the arguments for the URL.
          $args = $this->uwSidebarCreateUrlArguments(
            $sidebar_nid,
            'View the sidebar.'
          );

          // Add the message to the top of the node.
          $this->messenger->addMessage(
            $this->t(
              'This page has a sidebar attached. @text_with_link',
              $args
            )
          );
        }
    }
  }

  /**
   * A function to set title of sidebar to the title of the node reference.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface|mixed $entity_type
   *   The entity type.
   * @param \Drupal\node\NodeInterface $node
   *   The node interface entity.
   * @param \Drupal\Core\Form\FormInterface|mixed $form
   *   The form entity.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state entity.
   */
  public function uwCtSidebarNodeBuilder($entity_type, NodeInterface $node, $form, FormStateInterface $form_state) {

    // If there is an attached page, set the title of the sidebar
    // to the title of the page.
    if ($node->field_uw_attach_page->entity) {

      // Set the title of sidebar to be the title of the node reference.
      $title = $this->t('@node_title (sidebar)', ['@node_title' => $node->field_uw_attach_page->entity->getTitle()]);
      $node->setTitle($title);
    }
  }

  /**
   * Form validation for attach page for sidebar.
   *
   * @param array $form
   *   The complete form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function uwCtSidebarAttachPageValidation(array &$form, FormStateInterface $form_state): void {

    // Get the values from the form_state.
    $values = $form_state->getValues();

    // Define entity id.
    $entity_id = 0;

    // If the entity reference is not empty, meaning that there is an
    // entity entered, then ensure that we can add that entity.
    if (!empty($values['field_uw_attach_page'][0]['target_id'])) {

      // Get the reference node id.
      $attached_page_nid = $values['field_uw_attach_page'][0]['target_id'];

      if (!empty($values['path'][0]['source'])) {
        // Sidebar node path is like '/node/100'.
        $sidebar_node_path = $values['path'][0]['source'];

        // Make sure sidebar node path is not empty due to
        // substr(): Passing null to parameter #1 ($string)
        // of type string is deprecated.
        if (!empty($sidebar_node_path)) {
          // Get node id from sidebar node path.
          $sidebar_nid = substr($sidebar_node_path, 6);
          if (!is_null($sidebar_nid)) {
            // See function declaration for what is returned in function call.
            $entity_id = $this->uwService->getOrCheckAttachedSidebar($attached_page_nid, $sidebar_nid, 'check');
          }
        }

        // If the entity_id is greater than 0, means that the attached_page_nid
        // is already attached to some other sidebar, then display an error.
        if ($entity_id > 0) {

          // Get the arguments for a URL.
          $args = $this->uwSidebarCreateUrlArguments(
            $entity_id,
            'View the existing sidebar.'
          );

          // Set the form state error.
          $form_state->setErrorByName('field_uw_attach_page', $this->t('The selected page already has a sidebar attached. @text_with_link', $args));
        }
      }
    }
  }

  /**
   * A function to get the URL arguments about a sidebar.
   *
   * @param int $sidebar_nid
   *   The nid of a sidebar.
   * @param string $link_text
   *   The text to be displayed inside the link.
   *
   * @return array
   *   An array of the URL arguments
   */
  public function uwSidebarCreateUrlArguments(int $sidebar_nid, string $link_text): array {

    // Get the URL of the other sidebar that this attached_page_nid attached to.
    $url = Url::fromRoute('entity.node.canonical', ['node' => $sidebar_nid]);

    // Set the arguments for the URL.
    $args = [
      '@text_with_link' => Link::fromTextAndUrl($link_text, $url)->toString(),
    ];

    return $args;
  }

  /**
   * Preprocess a node with bundle type article in view mode full.
   *
   * @param \Drupal\preprocess_event_dispatcher\Event\NodePreprocessEvent $event
   *   Event.
   */
  public function preprocessNode(NodePreprocessEvent $event): void {

    /** @var \Drupal\preprocess_event_dispatcher\Variables\NodeEventVariables $variables */
    $variables = $event->getVariables();

    // Get the node object.
    $node = $variables->getNode();

    // The node types that the sidebar can be attached to.
    $node_types = $this->uwService->getUwContentTypes(TRUE);

    // Check if the node we are in can have sidebars.
    if (in_array($node->getType(), $node_types)) {

      // Get the sidebar nid that is attached to this page.
      $sidebar_nid = $this->uwService->getOrCheckAttachedSidebar($node->id());

      // If there is a sidebar exists.
      if ($sidebar_nid > 0) {
        // Get the node storage object.
        $storage = $this->entityTypeManager->getStorage('node');

        // Load the node from the entity_id from the sidebar.
        $sidebar_node = $storage->load($sidebar_nid);

        // Ensure that the sidebar node is published.
        if ($sidebar_node->get('moderation_state')->value === 'published') {

          // Get the build array.
          $build = $this->entityTypeManager->getViewBuilder('node')->view($sidebar_node, 'default');

          // Set the build array in the variables, so that the sidebar
          // can be displayed.
          $variables->set('sidebar', $build);
        }
      }
    }
  }

  /**
   * Invalidates attached page node cache.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity that is being saved.
   */
  public function invalidateCacheForAttachedPage(EntityInterface $entity): void {
    // Trigger only for node entity types, skips over other entities like
    // moderation_state and path_alias.
    if ($entity->getEntityTypeId() === 'node' && $entity->bundle() === 'uw_ct_sidebar') {

      // Get attached page entity.
      $attached_to = $entity->field_uw_attach_page->entity;

      // If there are tags associated with that entity, invalidate them.
      if ($attached_to && $cache_tags_in = $attached_to->getCacheTagsToInvalidate()) {
        Cache::invalidateTags($cache_tags_in);
      }
    }
  }

  /**
   * Hook when sidebar entity has been created.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityInsertEvent $event
   *   Event dispatched by hook_event_dispatcher.
   */
  public function invalidateCacheForAttachedPageOnInsert(EntityInsertEvent $event): void {
    $this->invalidateCacheForAttachedPage($event->getEntity());
  }

  /**
   * Hook when sidebar entity has been updated.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityUpdateEvent $event
   *   Event dispatched by hook_event_dispatcher.
   */
  public function invalidateCacheForAttachedPageOnUpdate(EntityUpdateEvent $event): void {
    $this->invalidateCacheForAttachedPage($event->getEntity());
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'hook_event_dispatcher.form_base_node_form.alter' => 'alterNodeForm',
      NodePreprocessEvent::name() => 'preprocessNode',
      EntityHookEvents::ENTITY_INSERT => 'invalidateCacheForAttachedPageOnInsert',
      EntityHookEvents::ENTITY_UPDATE => 'invalidateCacheForAttachedPageOnUpdate',
    ];
  }

}
